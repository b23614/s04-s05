package com.zuitt.activity;

public class Contact {
  public String name;
  public String contactNumber;
  public String address;

  public Contact(){};

  public Contact(String name, String contactNumber, String address){
    this.name = name;
    this.contactNumber = contactNumber;
    this.address = address;
  }

  //Getter
  public String getName(){ return this.name; }
  public String getContactNumber(){ return this.contactNumber; }
  public String getAddress(){ return this.address; }

  //Setter
  public void setName(String name){
    this.name = name;
  }
  public void setContactNumber(String contactNumber){
    this.contactNumber = contactNumber;
  }
  public void setAddress(String address){
    this.address = address;
  }

}
