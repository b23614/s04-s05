package com.zuitt.activity;
public class Main {
    public static void main(String[] args){
        Phonebook phonebook = new Phonebook();

        Contact contactOne = new Contact();
        contactOne.setName("John Doe");
        contactOne.setContactNumber("+639152468596");
        contactOne.setAddress("Quezon City");

        Contact contactTwo = new Contact();
        contactTwo.setName("Jane Doe");
        contactTwo.setContactNumber("+639162148573");
        contactTwo.setAddress("Caloocan City");

        phonebook.setContacts(contactOne);
        phonebook.setContacts(contactTwo);

        if(phonebook.getContacts().size() == 0 ){
            System.out.println("Phonebook is Empty");
        }else{
            phonebook.getContacts().forEach(contact -> {
                System.out.printf("-------------------%n%s%n-------------------%n%s has the following registered number:%n%s%n%s has the following registered address:%nmy home in %s%n", contact.name, contact.name, contact.contactNumber, contact.name, contact.address );
            });
        }
    }

}

